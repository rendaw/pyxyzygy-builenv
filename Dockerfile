FROM fedora:29
RUN yum install -y \
	gcc-c++ \
	libstdc++-devel libstdc++-static \
	glibc-devel glibc-static \
	libpng-devel libpng-static \
	zlib-devel zlib-static \
	maven \
	java-11-openjdk-devel java-11-openjdk-jmods \
	mingw64-gcc mingw64-gcc-c++ \
	mingw64-winpthreads mingw64-winpthreads-static \
	mingw64-libpng mingw64-libpng-static \
	mingw64-zlib mingw64-zlib-static \
	wget zip swig inkscape python3 python3-toml git
RUN curl -L https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip -o openjdk.zip
RUN echo 'cf39490fe042dba1b61d6e9a395095279a69e70086c8c8d5466d9926d80976d8 openjdk.zip' | sha256sum -c -
RUN unzip openjdk.zip
RUN curl -L https://broth.itch.ovh/butler/linux-amd64/15.8.0/archive/default -o butler.zip
RUN echo '385b203af6b9d6ea260f4049f259a3de28802537e78ac35d6390c8243cdf25bd butler.zip' | sha256sum -c -
RUN unzip -d butler butler.zip